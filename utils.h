#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <string.h>

const char* SERVER_IP = "127.0.0.1";  // Adresse localhost (interface interne)
#define SERVER_PORT 4000
#define LINE_SIZE 200
#define REPLY_SIZE 2000
#define DEBUG 0

#define NUM_USERS 2
#define MAX_CONCURRENT_USERS 1

//Renvoie un message d'erreur en cas de problème
void exit_msg(char* msg, int err) {
  fprintf(stderr, "Error - %s: ", msg);
  if (err) perror(NULL);
  exit(1);
}

