#include "utils.h"

int main(int argc, char **argv)
{
  int sockfd;
  struct sockaddr_in server_addr;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd == -1)
  {
    perror("Socket creation failed");
    exit(EXIT_FAILURE);
  }

  memset(&server_addr, 0, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = inet_addr(SERVER_IP);
  server_addr.sin_port = htons(SERVER_PORT);

  if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
  {
    perror("Connection failed");
    exit(EXIT_FAILURE);
  }

  char buffer[LINE_SIZE];
  int bytes_written, bytes_read;

  printf("Type your message (or 'exit' to quit): ");
  fgets(buffer, LINE_SIZE, stdin);
  buffer[strcspn(buffer, "\n")] = '\0';

  if (strcmp(buffer, "exit") == 0)
  {
    close(sockfd);
    return 0;
  }

  bytes_written = send(sockfd, buffer, strlen(buffer), 0);
  if (bytes_written == -1)
  {
    perror("Message send failed");
    close(sockfd);
    exit(EXIT_FAILURE);
  }

  printf("Message from server: \n\n");

  while (1)
  {
    bytes_read = recv(sockfd, buffer, LINE_SIZE, 0);
    if (bytes_read == 0)
    {
      printf("Connection closed by server.\n");
      break;
    }
    else if (bytes_read == -1)
    {
      perror("Receiving message failed");
      close(sockfd);
      exit(EXIT_FAILURE);
    }
    else
    {
      buffer[bytes_read] = '\0';
      printf("%s", buffer);
    }
  }

  printf("\n");
  close(sockfd);

  return 0;
}
