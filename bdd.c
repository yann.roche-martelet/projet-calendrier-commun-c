#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "bdd.h"
#include "utils.h"
#include <time.h>

// File containing the data
static const char *DATA_FILE = "data";

// Converts a Day enum to a string
char *day_to_string(enum Day day)
{
  switch (day)
  {
  case MON:
    return "Lundi";
  case TUE:
    return "Mardi";
  case WED:
    return "Mercredi";
  case THU:
    return "Jeudi";
  case FRI:
    return "Vendredi";
  case SAT:
    return "Samedi";
  case SUN:
    return "Dimanche";
  default:
    return "Invalid Day";
  }
}

// Converts a string to a Day enum
enum Day string_to_day(char *str)
{
  char lower_str[LINE_SIZE];
  strncpy(lower_str, str, LINE_SIZE);
  for (int i = 0; i < strlen(lower_str); i++)
  {
    lower_str[i] = tolower(lower_str[i]);
  }

  if (strcmp(lower_str, "lundi") == 0)
    return MON;
  if (strcmp(lower_str, "mardi") == 0)
    return TUE;
  if (strcmp(lower_str, "mercredi") == 0)
    return WED;
  if (strcmp(lower_str, "jeudi") == 0)
    return THU;
  if (strcmp(lower_str, "vendredi") == 0)
    return FRI;
  if (strcmp(lower_str, "samedi") == 0)
    return SAT;
  if (strcmp(lower_str, "dimanche") == 0)
    return SUN;

  return NONE;
}

// Frees memory allocated for a Data structure
void free_data(Data *data)
{
  free(data->name);
  free(data->activity);
  free(data);
}

// Formats data into a string
void format_data(char *line, const Data *data)
{
  snprintf(line, LINE_SIZE, "%s,%s,%s,%d\n",
           data->name, data->activity,
           day_to_string(data->day), data->hour);
}

// Parses a line of data into a Data structure
Data *parse_data(const char *line)
{
  Data *data = (Data *)malloc(sizeof(Data));
  char *parse;
  char error_message[LINE_SIZE];
  snprintf(error_message, LINE_SIZE, "Parsing error: %s\n", line);

  char *line_copy = strdup(line);

  parse = strtok(line_copy, ",");
  if (!parse)
    exit_msg(error_message, 0);
  data->name = strdup(parse);

  parse = strtok(NULL, ",");
  if (!parse)
    exit_msg(error_message, 0);
  data->activity = strdup(parse);

  parse = strtok(NULL, ",");
  if (!parse)
    exit_msg(error_message, 0);
  data->day = string_to_day(parse);

  parse = strtok(NULL, "\n");
  if (!parse)
    exit_msg(error_message, 0);
  data->hour = atoi(parse);

  free(line_copy);
  return data;
}
#define SIZE 30
// Checks if a user has an activity at a specific time
int user_activity_exists(const Data *data)
{
  FILE *file;
  char line[SIZE];
  file = fopen(DATA_FILE, "r");

  if (!file)
  {
    printf("File not found\n");
    return 0;
  }

  while (fgets(line, SIZE, file))
  {
    Data *data_from_file = parse_data(line);

    if (strcmp(data->name, data_from_file->name) == 0 &&
        data->day == data_from_file->day &&
        data->hour == data_from_file->hour)
    {
      fclose(file);
      free_data(data_from_file);
      return 1;
    }
    free_data(data_from_file);
  }
  fclose(file);
  return 0;
}

// Comparator function for sorting Data
int compare_data(const void *a, const void *b)
{
  Data *data_a = *(Data **)a;
  Data *data_b = *(Data **)b;

  if (data_a->day != data_b->day)
    return data_a->day - data_b->day;

  if (data_a->hour != data_b->hour)
    return data_a->hour - data_b->hour;

  return strcmp(data_a->name, data_b->name);
}

// Sorts the database and rewrites it
void sort_database()
{
  FILE *orig_db, *sorted_db;
  const char *SORTED_DATA_FILE = "data_sorted";
  char line[SIZE];
  Data *data_list[30];
  int count = 0;

  orig_db = fopen(DATA_FILE, "r");
  if (!orig_db)
  {
    printf("Error opening original database file\n");
    return;
  }

  while (fgets(line, SIZE, orig_db))
  {
    data_list[count++] = parse_data(line);
  }
  fclose(orig_db);

  sorted_db = fopen(SORTED_DATA_FILE, "w");
  if (!sorted_db)
  {
    printf("Error opening sorted database file\n");
    return;
  }

  qsort(data_list, count, sizeof(Data *), compare_data);

  for (int i = 0; i < count; i++)
  {
    char data_line[LINE_SIZE];
    format_data(data_line, data_list[i]);
    fprintf(sorted_db, "%s", data_line);
    free_data(data_list[i]);
  }
  fclose(sorted_db);

  if (remove(DATA_FILE) != 0)
  {
    printf("Error deleting original file\n");
    return;
  }

  if (rename(SORTED_DATA_FILE, DATA_FILE) != 0)
  {
    printf("Error renaming sorted file\n");
    return;
  }

  printf("Database sorted successfully.\n");
}

// Adds data to the database
void add_data(Data *data)
{
  FILE *file;
  char buffer[SIZE];
  int duplicate_found = 0;

  if (user_activity_exists(data))
  {
    printf("User already has an activity at the specified time.\n");
    free_data(data);
    return;
  }

  file = fopen(DATA_FILE, "a+");
  if (!file)
  {
    perror("Error opening file");
    free_data(data);
    return;
  }

  fseek(file, 0, SEEK_SET);

  while (fgets(buffer, SIZE, file) != NULL)
  {
    Data *existing_data = parse_data(buffer);
    if (strcmp(data->name, existing_data->name) == 0 &&
        strcmp(data->activity, existing_data->activity) == 0 &&
        data->day == existing_data->day &&
        data->hour == existing_data->hour)
    {
      duplicate_found = 1;
      printf("Duplicate entry found.\n");
      free_data(existing_data);
      break;
    }
    free_data(existing_data);
  }

  if (!duplicate_found)
  {
    fseek(file, 0, SEEK_END);
    char formatted_data[SIZE];
    format_data(formatted_data, data);
    fprintf(file, "%s", formatted_data);
    printf("Data added successfully.\n");
  }
  else
  {
    printf("Data already exists in the database.\n");
  }

  fclose(file);
  sort_database();
  free_data(data);
}

// Deletes data from the database
void delete_data(Data *data)
{
  FILE *file, *temp_file;
  char line[SIZE];
  int found = 0;

  file = fopen(DATA_FILE, "r");
  if (!file)
  {
    perror("Error opening data file");
    free_data(data);
    return;
  }

  temp_file = fopen("temp_data", "w");
  if (!temp_file)
  {
    perror("Error creating temporary file");
    fclose(file);
    free_data(data);
    return;
  }

  while (fgets(line, SIZE, file) != NULL)
  {
    Data *existing_data = parse_data(line);
    if (strcmp(data->name, existing_data->name) == 0 &&
        strcmp(data->activity, existing_data->activity) == 0 &&
        data->day == existing_data->day &&
        data->hour == existing_data->hour)
    {
      found = 1;
      printf("Found data to delete.\n");
    }
    else
    {
      fprintf(temp_file, "%s,%s,%s,%d\n", existing_data->name, existing_data->activity,
              day_to_string(existing_data->day), existing_data->hour);
    }
    free_data(existing_data);
  }

  fclose(file);
  fclose(temp_file);

  if (!found)
  {
    printf("Data to delete not found.\n");
    remove("temp_data");
  }
  else
  {
    if (remove(DATA_FILE) != 0)
    {
      perror("Error deleting original data file");
    }
    else if (rename("temp_data", DATA_FILE) != 0)
    {
      perror("Error renaming temporary file to data file");
    }
    else
    {
      printf("Data deleted successfully.\n");
    }
  }

  free_data(data);
}

// Displays the schedule based on the mode and argument
// Function to display user's schedule
// Function to display user's schedule
void display_user_schedule(const char *name)
{
  FILE *file;
  char line[SIZE];

  file = fopen(DATA_FILE, "r");
  if (!file)
  {
    perror("Error opening data file");
    return;
  }

  printf("Schedule for %s:\n", name);

  while (fgets(line, SIZE, file) != NULL)
  {
    Data *existing_data = parse_data(line);
    if (strcmp(name, existing_data->name) == 0)
    {
      printf("%s %dh: %s\n", day_to_string(existing_data->day), existing_data->hour, existing_data->activity);
    }
    free_data(existing_data);
  }

  fclose(file);
}
// Function to display all schedules
void display_all_schedules()
{
  FILE *file;
  char line[SIZE];

  file = fopen(DATA_FILE, "r");
  if (!file)
  {
    perror("Error opening data file");
    return;
  }

  printf("Full schedule:\n");

  while (fgets(line, SIZE, file) != NULL)
  {
    Data *existing_data = parse_data(line);
    printf("%s %dh: %s is doing %s\n", day_to_string(existing_data->day),
           existing_data->hour, existing_data->name, existing_data->activity);
    free_data(existing_data);
  }

  fclose(file);
}

// Function to display all schedules
// Function to display schedule for a specific day
void display_schedule(const char *mode, const char *arg)
{
  FILE *file;
  char line[SIZE];
  enum Day target_day;

  file = fopen(DATA_FILE, "r");
  if (!file)
  {
    perror("Error opening data file");
    return;
  }

  if (strcmp(mode, "day") == 0)
  {
    target_day = string_to_day(arg);
    if (target_day == NONE)
    {
      printf("Invalid day provided.\n");
      fclose(file);
      return;
    }
    printf("Schedule for %s:\n", arg);

    while (fgets(line, SIZE, file) != NULL)
    {
      Data *existing_data = parse_data(line);
      if (existing_data->day == target_day)
      {
        printf("%dh: %s is doing %s\n", existing_data->hour, existing_data->name, existing_data->activity);
      }
      free_data(existing_data);
    }
  }
  else
  {
    printf("Invalid mode provided. Use 'day'.\n");
  }

  fclose(file);
}

// Logs a command with a timestamp
void log_command(const char *command)
{
  FILE *log_file = fopen("data.log", "a");
  if (!log_file)
  {
    printf("Error opening log file\n");
    return;
  }

  time_t raw_time;
  struct tm *time_info;
  char time_str[80];

  time(&raw_time);
  time_info = localtime(&raw_time);

  strftime(time_str, sizeof(time_str), "%Y-%m-%d %H:%M:%S", time_info);
  fprintf(log_file, "[%s] %s\n", time_str, command);

  fclose(log_file);
}

int main(int argc, char **argv)
{
  if (argc < 2)
  {
    printf("Usage: %s <command> [arguments]\n", argv[0]);
    return 1;
  }

  char *command = argv[1];
  log_command(command);

  if (strcmp(command, "ADD") == 0)
  {
    if (argc != 6)
    {
      printf("Usage: %s ADD <name> <activity> <day> <hour>\n", argv[0]);
      return 1;
    }

    Data *data = (Data *)malloc(sizeof(Data));
    if (!data)
    {
      perror("Memory allocation error");
      return 1;
    }

    data->name = strdup(argv[2]);
    data->activity = strdup(argv[3]);

    data->day = string_to_day(argv[4]);
    if (data->day == NONE)
    {
      printf("Invalid day\n");
      free(data->name);
      free(data->activity);
      free(data);
      return 1;
    }

    data->hour = atoi(argv[5]);
    if (data->hour <= 0 || data->hour > 24)
    {
      printf("Invalid hour\n");
      free(data->name);
      free(data->activity);
      free(data);
      return 1;
    }

    add_data(data);
  }
  else if (strcmp(command, "DEL") == 0)
  {
    if (argc != 6)
    {
      printf("Usage: %s DEL <name> <activity> <day> <hour>\n", argv[0]);
      return 1;
    }

    Data *data = (Data *)malloc(sizeof(Data));
    if (!data)
    {
      perror("Memory allocation error");
      return 1;
    }

    data->name = strdup(argv[2]);
    data->activity = strdup(argv[3]);

    data->day = string_to_day(argv[4]);
    if (data->day == NONE)
    {
      printf("Invalid day\n");
      free(data->name);
      free(data->activity);
      free(data);
      return 1;
    }

    data->hour = atoi(argv[5]);
    if (data->hour <= 0 || data->hour > 24)
    {
      printf("Invalid hour\n");
      free(data->name);
      free(data->activity);
      free(data);
      return 1;
    }

    delete_data(data);
  }
  else if (strcmp(command, "SEE") == 0)
  {
    if (argc == 2)
    {
      // Display all schedules
      display_all_schedules();
    }
    else if (argc == 3)
    {
      // Check if the argument is a valid day
      enum Day day = string_to_day(argv[2]);
      if (day != NONE)
      {
        display_schedule("day", argv[2]);
      }
      else
      {
        // Assume it's a user name
        display_user_schedule(argv[2]);
      }
    }
    else
    {
      printf("Usage: %s SEE [<day>|<name>]\n", argv[0]);
      return 1;
    }
  }
  else
  {
    printf("Unknown command. Valid commands are: ADD, DEL, SEE\n");
    return 1;
  }

  return 0;
}
