#include "utils.h"
#include <pthread.h>
#include <semaphore.h>

char *bdd_bin = "./bdd";
sem_t *database_semaphore;

char **parse(char *line)
{
  char **res = malloc(7 * sizeof(char *));
  res[0] = bdd_bin;

  char *arg1 = strtok(line, " ");
  res[1] = arg1;

  char *arg2 = strtok(NULL, " ");
  res[2] = arg2;
  if (arg2 == NULL)
  {
    arg1[strlen(arg1) - 1] = '\0';
    return res;
  }

  char *arg3 = strtok(NULL, " ");
  res[3] = arg3;
  if (arg3 == NULL)
  {
    arg2[strlen(arg2) - 1] = '\0';
    return res;
  }

  char *arg4 = strtok(NULL, " ");
  res[4] = arg4;
  if (arg4 == NULL)
  {
    arg3[strlen(arg3) - 1] = '\0';
    return res;
  }

  char *arg5 = strtok(NULL, "\n");
  res[5] = arg5;
  res[6] = NULL;
  return res;
}

// Configuration de la socket réseau, retourne le file descriptor de la socket
int configure_socket()
{

  int socket_desc;
  struct sockaddr_in serv_addr;

  // Create a TCP/IP socket
  socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_desc == -1)
  {
    perror("Error creating socket");
    exit(EXIT_FAILURE);
  }

  // Initialize server address struct
  memset((char *)&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(SERVER_IP);
  serv_addr.sin_port = htons(SERVER_PORT);

  if (bind(socket_desc, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    perror("Error binding socket");
    exit(EXIT_FAILURE);
  }
  return socket_desc;
}


void process_communication(int socket_desc, pthread_t client_tid)
{
    char message[LINE_SIZE];
    int read_size;

    // Read only once from the client
    if ((read_size = recv(socket_desc, message, LINE_SIZE, 0)) > 0)
    {
        message[read_size] = '\0';
        char **args = parse(message);

        int fds[2];
        pipe(fds);
        pid_t pid = fork();

        if (pid == 0)
        { // Child Process
            close(fds[0]); // Close read end of the pipe
            dup2(fds[1], STDOUT_FILENO); // Allow stdout to write at the end of the pipe
            close(fds[1]); // Avoid redundancies or error (?)

            if (strcmp(args[1], "ADD") == 0)
            {
                printf("[%lu] Commande ADD effectuée\n", (unsigned long)client_tid);
            }
            else if (strcmp(args[1], "DEL") == 0)
            {
                printf("[%lu] Commande DEL effectuée\n", (unsigned long)client_tid);
            }
            else
            {
                printf("[%lu] Not recognized command, either ADD or DEL\n", (unsigned long)client_tid);
                exit(EXIT_FAILURE); // Exit child process if command is not recognized
            }

            execvp(args[0], args);
            perror("Error in exec command at database");
            exit(EXIT_FAILURE);
        }
        else
        { // Parent Process
            close(fds[1]); // Close write end of the pipe

            char response[LINE_SIZE]; // Increase the buffer size to accommodate larger responses
            int response_pos = 0;

            // Acquire the semaphore before accessing the shared resource
            printf("[%lu] Waiting for the semaphore...\n", (unsigned long)client_tid);
            if (sem_wait(database_semaphore) != 0)
            {
                perror("Error acquiring semaphore");
                exit(EXIT_FAILURE);
            }
            printf("[%lu] Database semaphore accessed.\n", (unsigned long)client_tid);

            ssize_t bytes_read;
            while ((bytes_read = read(fds[0], response + response_pos, LINE_SIZE)) > 0)
            {
                response_pos += bytes_read;
                response[response_pos] = '\0';
            }

            sleep(10);

            // Release the semaphore after accessing the shared resource
            if (sem_post(database_semaphore) != 0)
            {
                perror("Error releasing semaphore");
                exit(EXIT_FAILURE);
            }
            printf("[%lu] Database semaphore released.\n", (unsigned long)client_tid);
            printf("\n");

            if (send(socket_desc, response, strlen(response), 0) == -1)
            {
                perror("Error in send response to client");
                exit(EXIT_FAILURE);
            }

            close(fds[0]);
            free(args);
        }
    }
}


void *handle_client(void *arg)
{
    int new_socket = *((int *)arg);
    pthread_t client_tid = pthread_self(); // Get the thread ID (TID) as client thread ID

    process_communication(new_socket, client_tid);

    close(new_socket);
    pthread_exit(NULL);
    exit(EXIT_SUCCESS);
}


int main(int argc, char **argv)
{
    int socket_desc, new_socket;
    struct sockaddr_in serv_addr, cli_addr;
    socklen_t clilen;

    // Remove existing semaphore if it exists
    sem_unlink("/database_semaphore");

    database_semaphore = sem_open("/database_semaphore", O_CREAT | O_EXCL, 0644, 1);
    if (database_semaphore == SEM_FAILED)
    {
        perror("Error initializing semaphore");
        exit(EXIT_FAILURE);
    }

    // Configuration of the socket of the server
    socket_desc = configure_socket();

    // Receive connections from clients
    if (listen(socket_desc, 10) == -1)
    {
        perror("Error listening");
        exit(EXIT_FAILURE);
    }

    // Accept incoming connections and create a new thread for each
    while (1)
    {
        clilen = sizeof(cli_addr);
        new_socket = accept(socket_desc, (struct sockaddr *)&cli_addr, &clilen);
        if (new_socket < 0)
        {
            perror("Error on accept");
            exit(EXIT_FAILURE);
        }

        // Create a thread to handle the client
        pthread_t tid;
        int *socket_ptr = malloc(sizeof(int));
        if (socket_ptr == NULL)
        {
            perror("Error allocating memory for socket pointer");
            close(new_socket);
            continue;
        }
        *socket_ptr = new_socket;

        if (pthread_create(&tid, NULL, handle_client, (void *)socket_ptr) != 0)
        {
            perror("Error creating thread");
            free(socket_ptr);
            close(new_socket);
            continue;
        }
    }

    // Close the named semaphore
    sem_close(database_semaphore);
    sem_unlink("/database_semaphore");

    close(socket_desc);
    return 0;
}